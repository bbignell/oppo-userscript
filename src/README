Oppo Userscript
================================================================================

A simple userscript for Greasemonkey and Tampermonkey that tweaks the display
of Jalopnik and its subsites (Oppositelock, Detroit, Drive, and Releases).
Can also be used on other Gawker media sites - this has not been tested.

Provides the following tweaks:
* Shrinks the sidebar and expands the main content area
* Reduces the text sie of the headlines in the sidebar, more compact layout
* Changes usernames from gray to the orange colour in the Oppo logo
* Changes tags from gray to green + italics
* Tweaks all link colours
* Displays thumbnails in their native size
* Tweaks to image-only posts that don't align nicely with the thumbnail/leader image
* Fix for Jalopnik front page to prevent empty 300px ad box in bottom-left from overlaying content
* Fix for Jalopnik front page to resize promoted content container
* Fix for blog description/other links on Jalopnik front page
* Increase avatar, username, tag, and timestamp size in posts


Release Notes
================================================================================

v20
* Fixed build to ensure gear.png was being copied over, new includes version 4

v19
* Re-release of Oppo-X v5 as the new Oppo userscript

Oppo-X v5
* Complete rework
* Hosted css, png, and primary js files
* Added optional features: colours (enabled by default), bigger sidebar (disabled by default)
* Added options popup (gear img) in vanity footer

Oppo-X v4
* N/A

Oppo-X v3
* Added a separate colours css
* Fixed link to userscript in vanity footer
* Started optional feature management w/cookies

Oppo-X v2
* Added Google Map link to vanity footer

Oppo-X v1
* Created new css file

v18
* Fixed layout for new discussion styles on frontpage

v17
* Standardized headers on frontpage to h4
* Tweaked top-meta margins on frontpage

v16
* Broken, see v17

v15
* Background colour for avatar container to remove line behind transparent images

v14
* Some colour tweaks and link colours using http://colorschemedesigner.com/#0d31TpsuCw0w0
* Text size tweaks in post view
* Some margin tweaks
* Fix for the recommended content overlay

v13
* Vanity footer w/info links

v12
* Fixed avatar position

v11
* Fixed layout of media blocks in post intros
* Sidebar link colours to blue and gray
* Moved as many tweaks as possible to inject css instead of inline styles

v10
* Smaller avatars, 50x50px is just right
* Potential fix for the promoted content in the sidebar on the front page

v9
* Even bigger avatars, now 100x100 px

v8
* Increase avatar, username, tag, and timestamp size in posts
* More compact layout of headlines in sidebar
