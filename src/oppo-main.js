window.console = window.console || { log:function(){}, debug:function(){}, error:function(){} };
window.oppo = window.oppo || {    
    cookies: {
        colours: {
            name: "Oppo.Colours",
            selector: "#oppo-option-colours",
            enabled: true
        },
        biggerSidebar: {
            name: "Oppo.BiggerSidebar",
            selector: "#oppo-option-sidebar",
            enabled: false
        },
        sfwOnly: {
            name: "Oppo.SfwOnly",
            selector: "#oppo-option-sfw",
            value: 0
        },                
        createCookie: function(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        },
        readCookie: function(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(";");
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return null;
        },
        eraseCookie: function(name) {
            this.createCookie(name, "", -1);
        }
    },
    initFeatures: function() {
        //load cookies
        var colours = this.cookies.readCookie(this.cookies.colours.name);
        if (colours === null) {
            this.cookies.createCookie(this.cookies.colours.name, "1", 10000);
            colours = "1";
        }
        this.cookies.colours.enabled = (colours === "1");

        var sidebar = this.cookies.readCookie(this.cookies.biggerSidebar.name);
        this.cookies.biggerSidebar.enabled = (sidebar !== null);

        var sfwOnly = this.cookies.readCookie(this.cookies.sfwOnly.name);
        if (sfwOnly !== null) {
            this.cookies.sfwOnly.value = sfwOnly;
        }
    },    
    run: function() {
        //sizing of the sidebar and content section
        var four = $(".row .four.columns");
        var eight = $($(".row .eight.columns")[0]);
        var sidebarWidth = this.cookies.biggerSidebar.enabled ? 226 : 186; //size (150|190) + padding:0 18px
        var contentWidth = eight.closest(".row").width() - sidebarWidth - 72; //add padding 18px * 2 for each side (or something)
        var sidebar = $(".sidebar-content");
        var headlines = sidebar.find(".just-headlines");
        
        var shrink = [four, sidebar, headlines];
        var grow = [eight];
        $(shrink).each(function(){$(this).width(sidebarWidth);});
        $(grow).each(function(){$(this).width(contentWidth);});
        
        //get all the post wrappers on the page (not articles)
        var posts = $(".post-wrapper .frontpage-item");
        //set your images free! makes the thumbs next to post natural size - simple left-aligned images only for now
        var mm = posts.find(".twelve.columns .twelve.columns.item-content .main-media");
        mm.css({"width":"auto"}).find("img").removeAttr("width").removeAttr("height");
        //standardize the header sizes to h4
        posts.find("h1.headline").removeClass("h1 h2 h3 h5").addClass("h4");

        //fix the layout of the has-media paragraphs that overlap thumbs
        //fix 640-wide images in media posts
        posts.each(function(){
            var media640 = $(this).find("p.has-media.media-640");
            media640.first().before("<div class='clear-both'> </div>");
        });
        //fix 300-wide images in media posts
        posts.each(function(){        
            var media300 = $(this).find("p.has-media.media-300");
            media300.first().before("<div class='clear-space'> </div>");        
        });

        //make the 300px ad block on bottom-left of the front page transparent, ads still show
        var ad300 = sidebar.find("#ad-300x-placeholder");
        ad300.css({"background-color":"transparent"}).removeClass("ad300x-placeholder");
         
        this.createFooter();
        $(document).on("click", "a.open-new", function(){
            window.open($(this).attr("href")); return false;
        }); 
        $(document).on("click", "a.option-gear", function(){
            window.oppo.toggleOptionPanel(this);
            return false;
        });         

        this.createOptionsPanel();      
    },
    createFooter: function() {
        //create a vanity footer
        var vanity = "<div class='clear-both'> </div><div class='eight columns push-four text-center'>";
        vanity += "<ul class='list list-text text-light proxima vanity'>";
        vanity += "<li>Oppo Userscript v" + GM_info.script.version + " by <a class='open-new' href='http://benjamin-bignell.kinja.com'>BJ</a></li>";
        vanity += "<li><a class='open-new' href='http://userscripts.org/scripts/show/183596'>Install or Update</a></li>";
        vanity += "<li><a class='open-new' href='https://bitbucket.org/bbignell/oppo-userscript'>Contribute or Report Bugs</a></li>";
        vanity += "<li><a class='open-new' href='https://mapsengine.google.com/map/embed?mid=z9VrWhy4yVYo.kDLJ9Pq4Tqu0'>Member Map</a></li>";        
        vanity += "<li><a class='option-gear' href='#' title='Options'><img src='' style='display:none'/></a></li>";
        vanity += "</ul></div>";
        $(".row.site-footer").append(vanity);
        $(".option-gear img").attr("src", GM_getResourceURL("oppoGearImg")).show();

    },
    createOptionsPanel: function() {
        var o1 = this.cookies.colours.selector.substring(1);
        var o2 = this.cookies.biggerSidebar.selector.substring(1);
        var o3 = this.cookies.sfwOnly.selector.substring(1);
        var options = "<div class='oppo-options'><h5>Oppo Userscript Options</h5><ul>";
        options += "<li><input type='checkbox' id='" + o1 + "'/><label class='proxima' for='" + o1 + "'>Colours";
        options += "<div class='colours orange'> </div><div class='colours green'> </div><div class='colours blue'> </div></label></li>";
        options += "<li><input type='checkbox' id='" + o2 + "'/><label class='proxima' for='" + o2 + "'>Bigger sidebar&nbsp;&nbsp;[ ---- ]</li>";
        
        options += "<li><input type='radio' id='" + o3 + "-0' name='" + o3 + "' value='0'/><label class='proxima' for='" + o3 + "-0'>On</label>";
        options += "<input type='radio' id='" + o3 + "-1' name='" + o3 + "' value='1'/><label class='proxima' for='" + o3 + "-1'>Masked</label>";
        options += "<input type='radio' id='" + o3 + "-2" + "' name='" + o3 + "' value='2'/><label class='proxima' for='" + o3 + "-2'>Off</label>";
        options += "<div class='colours'><img src='" + GM_getResourceURL("oppoBreastImg") + "'/></div></li>";
        
        options += "</ul>";
        options += "<button class='cancel'>Cancel</button><button class='save'>Save</button></div>";
        $("body").append(options);
       
        $(".oppo-options .cancel").on("click", function(){
            window.oppo.hideOptionsPanel();
            return false;
        });
        
        $(".oppo-options .save").on("click", function(){
            window.oppo.saveOptions();
            window.oppo.hideOptionsPanel();
            window.location.reload();
            return false;
        });
    },
    saveOptions: function() {
        var colours = this.getOptionChecked(this.cookies.colours);
        if (colours === 1) {
            this.cookies.createCookie(this.cookies.colours.name, "1", 10000);
        } else {
            this.cookies.createCookie(this.cookies.colours.name, "0", 10000);
        }

        var sidebar = this.getOptionChecked(this.cookies.biggerSidebar);
        if (sidebar === 1) {
            this.cookies.createCookie(this.cookies.biggerSidebar.name, "1", 10000);
        } else {
            this.cookies.eraseCookie(this.cookies.biggerSidebar.name);
        }

        var sfw = this.getOptionRadio(this.cookies.sfwOnly);
        if (sfw !== this.cookies.sfwOnly.value) {
            this.cookies.eraseCookie(this.cookies.sfwOnly.name);
            this.cookies.createCookie(this.cookies.sfwOnly.name, sfw, 10000);
        }
    },
    toggleOptionPanel: function(target) {
        if ($(".oppo-options:visible").length === 0) {
            this.showOptionsPanel(target);
        } else {
            this.hideOptionsPanel();
        }
    },
    hideOptionsPanel: function() {
        $(".oppo-options").fadeOut("fast");
    },
    showOptionsPanel: function(target) {
        this.setOptionCheckbox(this.cookies.colours);
        this.setOptionCheckbox(this.cookies.biggerSidebar);
        this.setOptionRadio(this.cookies.sfwOnly);

        var panel = $(".oppo-options");
        if (target === null) {
            panel.css({"bottom":"30px","right":"0px"}).show();
        } else {
            var t = $(target);  
            var bottom = "30px";
            var right = ($(document).width() - t.offset().left - t.width() - 5) + "px";
            panel.css({"bottom":"30px","right":right}).fadeIn("fast");
        }
    },
    setOptionCheckbox: function(cookie) {
        if (cookie.enabled) {
            $(cookie.selector).attr("checked", "checked");
        } else {
            $(cookie.selector).removeAttr("checked");
        }
    },
    getOptionChecked: function(cookie) {
        return $(cookie.selector + ":checked").length;
    },
    setOptionRadio: function(cookie) {
        $("input[name=" + cookie.selector.substring(1) + "]").filter("[value=" + cookie.value + "]").prop("checked", true);
    },
    getOptionRadio: function(cookie) {
        return $("input[name=" + cookie.selector.substring(1) + "]:checked").val();
    }
};